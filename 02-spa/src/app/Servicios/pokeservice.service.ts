import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class PokeserviceService {
  apiUrl = 'https://pokeapi.co/api/v2/pokemon'; 

  constructor(private _http:HttpClient) { }

  getPokemons(){
    return this._http.get<pokemons[]>(this.apiUrl);
  }

}

export interface pokemons{
  count: number;
  next: string;
  previous: null;
  results: Array<resultados>;
}

export interface resultados{
  name: string;
  url: string;
}
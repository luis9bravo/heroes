import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService } from '../../Servicios/heroes.service';

@Component({
  selector: 'app-buscador-heore',
  templateUrl: './buscador-heore.component.html',
  styleUrls: ['./buscador-heore.component.css']
})
export class BuscadorHeoreComponent implements OnInit {
  heroes: any[] = [];
  termini:string;
  constructor( private activatedRoute: ActivatedRoute,
               private heroesService: HeroesService ) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {
      this.termini = params['termino'];
      this.heroes = this.heroesService.buscarHeroe(params['termino']);
      console.log(this.heroes);
    });
  

  }

}

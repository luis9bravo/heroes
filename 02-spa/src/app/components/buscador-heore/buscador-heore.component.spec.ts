import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscadorHeoreComponent } from './buscador-heore.component';

describe('BuscadorHeoreComponent', () => {
  let component: BuscadorHeoreComponent;
  let fixture: ComponentFixture<BuscadorHeoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuscadorHeoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscadorHeoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

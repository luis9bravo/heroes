import { Component, OnInit } from '@angular/core';
import { PokeserviceService, pokemons } from '../../Servicios/pokeservice.service';

@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex.component.html',
  styleUrls: ['./pokedex.component.css']
})
export class PokedexComponent implements OnInit {
  poke$: any[] = [];

  constructor(private pokeserviceService:PokeserviceService) { }

  ngOnInit() {
    

    // this.poke$ = this.pokeserviceService.getPokemons();
    // return this.pokeserviceService.getPokemons().subscribe((data) =>{
    //   //this.poke$ = this.pokeserviceService.getPokemons();
    //   var hola = JSON.stringify(data);
    //   this.poke$ = data;
    //   console.log(hola);
    //   console.log(data);
    // });

    this.pokeserviceService.getPokemons().subscribe((data) => { // Success
        this.poke$ = data['results'];
        console.log(this.poke$);
      }
    );
  }
  
}
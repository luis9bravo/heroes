import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {AboutComponent} from './components/about/about.component';
import {HeroesComponent} from './components/heroes/heroes.component';
import {BuscadorHeoreComponent} from './components/buscador-heore/buscador-heore.component';
import {HeroeComponent} from './components/heroe/heroe.component';
import {PokedexComponent} from './components/pokedex/pokedex.component';

const APP_ROUTES: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'about', component: AboutComponent},
    {path: 'heores', component: HeroesComponent},
    {path: 'poke', component: PokedexComponent},
    {path: 'heroe/:id', component: HeroeComponent},
    {path: 'buscar/:termino', component: BuscadorHeoreComponent},
    {path: '**', pathMatch: 'full', redirectTo:'home'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);